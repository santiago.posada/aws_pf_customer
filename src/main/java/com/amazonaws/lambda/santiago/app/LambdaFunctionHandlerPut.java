package com.amazonaws.lambda.santiago.app;

import com.amazonaws.lambda.santiago.app.dbo.CustomerEntity;
import com.amazonaws.lambda.santiago.app.dto.Request;
import com.amazonaws.lambda.santiago.app.exception.GeneralException;

import com.amazonaws.lambda.santiago.app.mapper.DynamoDataBaseMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


public class LambdaFunctionHandlerPut implements RequestHandler<Request, Object> {


    @Override
    public Object handleRequest(Request request, Context context) {

        DynamoDBMapper mapper = new DynamoDataBaseMapper().getMapper();
        CustomerEntity customer =mapper.load(CustomerEntity.class,request.getIdentity_document());

        if (customer==null){

            throw new GeneralException("Customer not found");

        }else{

            CustomerEntity customerPut= request.getCustomer();
            customerPut.setIdentity_document(request.getIdentity_document());
            mapper.save(customerPut);

            return customerPut;
        }




    }

}
