package com.amazonaws.lambda.santiago.app.mapper;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class DynamoDataBaseMapper {

    private AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
    private DynamoDBMapper mapper = new DynamoDBMapper(db);

    public DynamoDataBaseMapper() {
    }

    public DynamoDBMapper getMapper() {
        return mapper;
    }

}
