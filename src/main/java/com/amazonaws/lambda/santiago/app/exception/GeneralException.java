package com.amazonaws.lambda.santiago.app.exception;

public class GeneralException extends  RuntimeException{


    private static final long serialVersionUID=1L;

    public GeneralException(String msg){
        super(msg);
    }

}
