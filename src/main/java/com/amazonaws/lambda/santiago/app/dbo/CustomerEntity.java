package com.amazonaws.lambda.santiago.app.dbo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "aws_pf_customer")
public class CustomerEntity {

    @DynamoDBHashKey
    private Long identity_document;

    @DynamoDBAttribute
    private Integer age;

    @DynamoDBAttribute
    private Integer id_city;

    @DynamoDBAttribute
    private Integer id_identity_document;

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    private String last_name;

    @DynamoDBAttribute
    private String mail;

    public CustomerEntity() {
    }

    public CustomerEntity(Long identity_document, Integer age, Integer id_city, Integer id_identity_document, String name, String last_name, String mail) {
        this.identity_document = identity_document;
        this.age = age;
        this.id_city = id_city;
        this.id_identity_document = id_identity_document;
        this.name = name;
        this.last_name = last_name;
        this.mail = mail;
    }

    public Long getIdentity_document() {
        return identity_document;
    }

    public void setIdentity_document(Long identity_document) {
        this.identity_document = identity_document;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId_city() {
        return id_city;
    }

    public void setId_city(Integer id_city) {
        this.id_city = id_city;
    }

    public Integer getId_identity_document() {
        return id_identity_document;
    }

    public void setId_identity_document(Integer id_identity_document) {
        this.id_identity_document = id_identity_document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
