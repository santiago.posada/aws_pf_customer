package com.amazonaws.lambda.santiago.app.dto;

import com.amazonaws.lambda.santiago.app.dbo.CustomerEntity;

public class Request {

    private Long identity_document;

    private CustomerEntity customer;

    public Long getIdentity_document() {
        return identity_document;
    }

    public void setIdentity_document(Long identity_document) {
        this.identity_document = identity_document;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }
}
